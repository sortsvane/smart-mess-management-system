function fetchVisitors() {
    $.getJSON("http://localhost:1199/all_visitors", function(visitors) {
      var tbody = $("#visitors tbody");
      tbody.empty();
  
      visitors.forEach(function(visitor) {
        var row = $("<tr>");
       .append($("<td>").text(visitor.card_id));
        row.append($("<td>").text(visitor.name));
        row.append($("<td>").text(visitor.due_amount));
        tbody.append(row);
      });
  
      // Update total sales for today
      var todaySales = visitors.reduce(function(sum, visitor) { return sum + visitor.due_amount; }, 0);
      $("#total_sales").text("$" + todaySales.toFixed(2));
    });
  }
  
  // Fetch visitors data on page load
  $(document).ready(function() {
    fetchVisitors();
  });