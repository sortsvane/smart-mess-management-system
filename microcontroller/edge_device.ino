#include <ESP8266WiFi.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <MFRC522.h>
#include <ArduinoJson.h>

// Constants
const char* ssid = "EACCESS"; // WiFi SSID
const char* password = "hostelnet"; // WiFi password
const int SDA_PIN = D2;
const int SCL_PIN = D1;
const int SS_PIN = D4;
const int RST_PIN = D3;
const int OLED_ADDR = 0x3C;

// Objects
WiFiClient wifiClient;
Adafruit_SSD1306 oled(128, 64, &Wire);
MFRC522 rfid(SS_PIN, RST_PIN);

void setup() {
  Serial.begin(115200);
  Wire.begin(SDA_PIN, SCL_PIN);
  oled.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  SPI.begin();
  rfid.PCD_Init();

  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to WiFi");

  // Display "Welcome" message on OLED
  oled.clearDisplay();
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  oled.setCursor(0, 0);
  oled.println("Welcome!");
  oled.display();
}

String apiGetRequest(String url) {
  // Get request to the API
  HTTPClient http;
  http.begin(url);
  int httpCode = http.GET();
  String payload = "";
  if (httpCode > 0) {
    payload = http.getString();
  }
  http.end();
  return payload;
}


void loop() {
  // Check for RFID card
  if (!rfid.PICC_IsNewCardPresent() || !rfid.PICC_ReadCardSerial())
    return;

  Serial.print("Card UID: ");
  for (byte i = 0; i < rfid.uid.size; i++) {
    Serial.print(rfid.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(rfid.uid.uidByte[i], HEX);
  }
  Serial.println();

  // If found, fetch user details via API and display on OLED
  String cardId = "";
  for (byte i = 0; i < rfid.uid.size; i++) {
    cardId += String(rfid.uid.uidByte[i], HEX);
  }

  String api_url = "http://192.168.0.31:1199/card_details/" + cardId;
  String response = apiGetRequest(api_url);

  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, response);
  if(!error) {
    const char* name = doc["name"];
    float due_amount = doc["due_amount"];
    oled.clearDisplay();
    oled.setCursor(0, 0);
    oled.println("Name: ");
    oled.print(name);
    oled.println();
    oled.println("Due Amount: ");
    oled.print(due_amount);
    oled.display();
  }

  // Log the visit via another API request
  String logUrl = "http://your_local_server_ip:port/log_visit";
  String logResponse = apiGetRequest(logUrl);

  delay(5000);

  // Display "Welcome" message on OLED again
  oled.clearDisplay();
  oled.setCursor(0, 0);
  oled.println("Welcome!");
  oled.display();
}