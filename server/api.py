from flask import Flask, jsonify, request
import redis

app = Flask(__name__)
redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)

@app.route("/card_details/<card_id>")
def get_card_details(card_id):
    # Fetch user details from Redis and return as JSON
    name = redis_db.hget(card_id, "name")
    due_amount = float(redis_db.hget(card_id, "due_amount"))

    if name and due_amount:
        return jsonify({"name": name.decode("utf-8"), "due_amount": due_amount})
    else:
        return jsonify({"error": "Card not found"}), 404

@app.route("/log_visit", methods=["POST"])
def log_visit():
    # Record visit in Redis
    card_id = request.json["card_id"]
    visitor_count_key = "visitors_count"
    visitor_key = f"visitor:{redis_db.get(visitor_count_key)}"

    if redis_db.hexists(card_id, "name"):
        redis_db.incr(visitor_count_key)
        redis_db.hmset(visitor_key, {"card_id": card_id})

        return jsonify({"success": True})
    else:
        return jsonify({"error": "Card not found"}), 404

@app.route("/all_visitors")
def get_all_visitors():
    # Fetch all visitors from Redis and return as JSON
    visitor_count = int(redis_db.get("visitors_count"))
    visitors = []

    for i in range(visitor_count):
        visitor_key = f"visitor:{i}"
        card_id = redis_db.hget(visitor_key, "card_id").decode("utf-8")
        name = redis_db.hget(card_id, "name").decode("utf-8")
        due_amount = float(redis_db.hget(card_id, "due_amount"))

        visitors.append({"card_id": card_id, "name": name, "due_amount": due_amount})

    return jsonify(visitors)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)