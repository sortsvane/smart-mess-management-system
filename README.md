Here is an outline of the code for each part of our project:

## Microcontroller
ESP8266 Arduino Sketch:
1. Import necessary libraries
2. Define constants (WiFi SSID, password, OLED pins, RC522 pins, etc.)
3. Initialize WiFi, OLED, and RC522 objects
4. Connect to WiFi and show the welcome message on OLED
5. In the loop, check for RFID card
6. If the card is found, query the API to fetch the user details and display them on OLED
7. Record the visit via another API request

## Server
Python Backend API:
1. Import necessary libraries (Flask, Redis, etc.)
2. Connect to Redis instance
3. Define Flask routes for required endpoints (Get card details, log visit, fetch all visitors)
4. Implement business logic in each route
5. Start the Flask app

## Admin Panel
HTML/CSS/JS Admin Frontend:
1. Create a basic HTML structure using Bootstrap for styling
2. Add a table to show visitors' details
3. Write JavaScript code to fetch the visitors via API and display them in the table
4. Add any additional UI elements for better user experience (e.g., search, pagination)